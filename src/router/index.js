import Vue from 'vue';
import Router from 'vue-router';
import Login from '@/components/Login';
import CrearMensaje from '@/components/CrearMensaje';
import Historial from '@/components/Historial';
import firebase from 'firebase';

Vue.use(Router);

const router = new Router({
  routes: [
    {
      path: '*',
      name: 'Login',
      component: Login
    },
    {
      path: '/home',
      name: 'CrearMensaje',
      component: CrearMensaje,
      meta:{
        autentificado: true
      }
    },
    {
      path: '/historial',
      name: 'Historial',
      component: Historial,
      meta:{
        autentificado: true
      }
    },
  ]
});

router.beforeEach((to,from,next) => {
  let usuario = firebase.auth().currentUser;
  let autorizacion = to.matched.some(record => record.meta.autentificado);

  if (autorizacion && !usuario) {
    return next('login');
  } else if (!autorizacion && usuario) {
    return next('home');
  }
  next();
});

export default router;