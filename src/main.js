// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'bootstrap-css-only/css/bootstrap.min.css';
import 'mdbvue/build/css/mdb.css';
import Vue from 'vue';
import App from './App';
import router from './router';
import firebase from 'firebase';

Vue.config.productionTip = false;

var config = {
  apiKey: "AIzaSyCYG0KD7K1vzNqVb-KqNUF2pbA9NXalUig",
  authDomain: "vueapp-ec5c3.firebaseapp.com",
  databaseURL: "https://vueapp-ec5c3.firebaseio.com",
  projectId: "vueapp-ec5c3",
  storageBucket: "vueapp-ec5c3.appspot.com",
  messagingSenderId: "245460769077"
};
firebase.initializeApp(config);
firebase.auth().onAuthStateChanged(function(user){
  /* eslint-disable no-new */
  new Vue({
    el: '#app',
    router,
    components: { App },
    template: '<App/>'
  });
});

